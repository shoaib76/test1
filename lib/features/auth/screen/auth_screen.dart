//import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:test1/common/widget/custom_button.dart';
import 'package:test1/common/widget/custom_radio.dart';
import 'package:test1/features/auth/service/auth_services.dart';

import '../../../common/widget/custom_textfiled.dart';

class AuthScreen extends StatefulWidget {
  static const String routeName = '/auth-screen';
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _signUpFormKey = GlobalKey<FormState>();
  final _signInFormKey = GlobalKey<FormState>();
  final TextEditingController _namecontroller = TextEditingController();
  final TextEditingController _emailcontroller = TextEditingController();
  final TextEditingController _passwordcontroller = TextEditingController();
  AuthServices authServices = AuthServices();
  Auth _groupvalue = Auth.signup;

  ValueChanged _valueChangedHandler() {
    return (value) => setState(() => _groupvalue = value!);
  }

  @override
  void dispose() {
    super.dispose();
    _namecontroller.dispose();
    _emailcontroller.dispose();
    _passwordcontroller.dispose();
  }

  void signUpUser() {
    authServices.signUpUser(
        context: context,
        name: _namecontroller.text,
        email: _emailcontroller.text,
        password: _passwordcontroller.text);
  }

  void signInUser() {
    authServices.signInUser(
      context: context,
       email: _emailcontroller.text,
        password: _passwordcontroller.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(children: [
            const Text(
              'Welcome',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
            ),
            CustomRadio(
              value: Auth.signup,
              groupvalue: _groupvalue,
              color: _groupvalue == Auth.signup ? Colors.white10 : Colors.white,
              text: 'Create Account',
              onchanged: _valueChangedHandler(),
            ),
            if (_groupvalue == Auth.signup)
              Container(
                padding: const EdgeInsets.all(8),
                color: Colors.white54,
                child: Form(
                  key: _signUpFormKey,
                  child: Column(children: [
                    CustomTextForm(
                        hinttext: 'name', controller: _namecontroller),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomTextForm(
                        hinttext: 'email', controller: _emailcontroller),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomTextForm(
                        hinttext: 'password', controller: _passwordcontroller),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomButton(
                        onTap: () {
                          if (_signUpFormKey.currentState!.validate()) {
                            signUpUser();
                          }
                        },
                        text: 'Signup'),
                  ]),
                ),
              ),
            if (_groupvalue == Auth.signin)
              Container(
                padding: const EdgeInsets.all(8),
                color: Colors.white60,
                child: Form(
                  key: _signInFormKey,
                  child: Column(children: [
                    CustomTextForm(
                        hinttext: 'email', controller: _emailcontroller),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomTextForm(
                        hinttext: 'password', controller: _passwordcontroller),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomButton(
                        onTap: () {
                          if (_signInFormKey.currentState!.validate()) {
                            signInUser();
                          }
                        },
                        text: 'Signin'),
                  ]),
                ),
              ),
            CustomRadio(
              value: Auth.signin,
              groupvalue: _groupvalue,
              color: _groupvalue == Auth.signin ? Colors.white10 : Colors.white,
              text: 'SignIn',
              onchanged: _valueChangedHandler(),
            ),
          ]),
        ),
      ),
    );
  }
}
