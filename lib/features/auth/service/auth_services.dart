import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test1/constants/error_handling.dart';
import 'package:test1/constants/utils.dart';
import 'package:test1/model/user.dart';
import 'package:http/http.dart' as http;

import '../../../constants/global_variables.dart';

class AuthServices {
  void signUpUser({
    required BuildContext context,
    required String email,
    required String password,
    required String name,
  }) async {
    try {
      User user = User(
          id: '',
          email: email,
          name: name,
          password: password,
          address: '',
          type: '',
          token: '');
      http.Response res = await http.post(
        Uri.parse('$uri/api/signup'),
        body: user.toJson(),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () {
          showSanckBar(
            context,
            'Account Created! Login with the same credentials!',
          );
        },
      );
    } catch (e) {
      showSanckBar(context, e.toString());
    }
  }

  void signInUser({
    required BuildContext context,
    required String email,
    required String password,
  }) async {
    try {
      http.Response res = await http.post(
        Uri.parse('$uri/api/signin'),
        body: jsonEncode({
          'email':email,
          'password': password,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () {
          showSanckBar(
            context,
            'Account Login successful',
          );
        },
      );
    } catch (e) {
      showSanckBar(context, e.toString());
    }
  }
}
