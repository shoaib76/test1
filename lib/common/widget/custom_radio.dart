import 'package:flutter/material.dart';

enum Auth {
  signin,
  signup,
}

class CustomRadio extends StatelessWidget {
  final Auth value;
  final Auth groupvalue;
  final Color color;
  final String text;
  final ValueChanged onchanged;

  const CustomRadio(
      {Key? key,
      required this.value,
      required this.groupvalue,
      required this.color,
      required this.text,
      required this.onchanged,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: color,
      title: Text(
        text,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      leading: Radio(
        activeColor: Colors.blue,
        value: value,
        groupValue: groupvalue,
        onChanged: onchanged,
      ),
    );
  }
}
