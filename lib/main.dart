import 'package:flutter/material.dart';

import '../features/auth/screen/auth_screen.dart';
import 'route.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      title: 'Fluo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        appBarTheme: const AppBarTheme(
          elevation: 0,
          color: Colors.blue,
        )
      ),
      onGenerateRoute: (settings) => genrateRoute(settings),
      home: const AuthScreen(),
    );
  }
}

